package com.upwork.exeption;

public class SpotCannotBeChosenException extends RuntimeException {

    public SpotCannotBeChosenException(String message) {
        super(message);
    }

}
