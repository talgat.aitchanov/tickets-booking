package com.upwork.exeption;

public class NoFreeSpotsException extends RuntimeException {

    public NoFreeSpotsException(String message) {
        super(message);
    }

}
