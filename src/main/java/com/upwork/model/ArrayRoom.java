package com.upwork.model;

import com.upwork.exeption.NoFreeSpotsException;
import com.upwork.exeption.SpotCannotBeChosenException;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ArrayRoom {

    private static final int DEFAULT_ROWS = 10;
    private static final int DEFAULT_COLUMNS = 10;
    private final int[][] planning = new int[DEFAULT_ROWS][DEFAULT_COLUMNS];
    private int currentRow = 0;
    private int currentAvailableSeat = 0;
    private int freeSpots = DEFAULT_ROWS * DEFAULT_COLUMNS;

    public int getRowCount() {
        return this.planning.length;
    }

    public int getTotalSeats() {
        return DEFAULT_ROWS * DEFAULT_COLUMNS;
    }

    public int getFreeSpots() {
        return this.freeSpots;
    }

    public int[] reserve(int row, int column) {
        if (freeSpots <= 0) {
            throw new NoFreeSpotsException("No free spots");
        }

        if (planning[row][column] != 0) {
            throw new SpotCannotBeChosenException("No free spots");
        }

        planning[row][column] = 1;
        deductOneSpot();
        if (isLeftSeatBooked(row, column)) {
            deductOneSpot();
        }
        if (isRightSeatBooked(row, column)) {
            deductOneSpot();
        }

        return new int[]{row, column};
    }

    private void deductOneSpot() {
        this.freeSpots--;
    }

    private boolean isRightSeatBooked(int row, int column) {
        if (column + 1 < DEFAULT_COLUMNS) {
            planning[row][column + 1] = -1;
            return true;
        }
        return false;
    }

    private boolean isLeftSeatBooked(int row, int column) {
        if (column - 1 >= 0) {
            planning[row][column - 1] = -1;
            return true;
        }
        return false;
    }

    public int[] reserve() {
        if (currentAvailableSeat >= planning[currentRow].length && currentRow < DEFAULT_ROWS - 1) {
            currentAvailableSeat = 0;
            currentRow++;
        }

        if (freeSpots <= 0) {
            throw new NoFreeSpotsException("No free spots");
        }

        int[] reservedSpot = {currentRow, currentAvailableSeat};
        planning[currentRow][currentAvailableSeat] = planning[currentRow][currentAvailableSeat] + 1;
        planning[currentRow][currentAvailableSeat + 1] = -1;
        currentAvailableSeat += 2;
        this.freeSpots -= 2;
        return reservedSpot;
    }

    public int[][] getPlanning() {
        return this.planning;
    }

    public void printSeats() {
        System.out.println(" \\ " + IntStream.range(0, 10).mapToObj(String::valueOf).collect(Collectors.joining(" ", "(", ")")));
        for (int i = planning.length - 1; i >= 0; i--) {
            for (int j = 0; j < planning[i].length; j++) {
                String row = j == 0 ? "(" + i + ") " : " ";
                System.out.print(row + planning[i][j]);
            }
            System.out.println();
        }
        System.out.println(" / " + IntStream.range(0, 10).mapToObj(String::valueOf).collect(Collectors.joining(" ", "(", ")")));
    }

}
