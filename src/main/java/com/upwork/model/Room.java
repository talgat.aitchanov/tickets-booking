package com.upwork.model;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private final int rowsCount;
    private final int columnsCount;
    private final List<Seat> seats;

    public Room(int rowsCount, int columnsCount) {
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.seats = initializeSeats(this.rowsCount * this.columnsCount);
    }

    public Room() {
        this.rowsCount = 10;
        this.columnsCount = 10;
        this.seats = initializeSeats(this.rowsCount * this.columnsCount);
    }

    private List<Seat> initializeSeats(int seats) {
        return new ArrayList<>(seats);
    }

    public int getRows() {
        return this.rowsCount;
    }

    public int getColumns() {
        return this.columnsCount;
    }

    public int getTotalSeats() {
        return seats.size();
    }

    public List<Seat> getSeats() {
        return this.seats;
    }
}
