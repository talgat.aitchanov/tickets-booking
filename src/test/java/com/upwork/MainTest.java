package com.upwork;

import com.upwork.model.Seat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {

    @Test
    void createSeat() {
        Seat seat = new Seat(0, 0);
        assertEquals(0, seat.getRow());
        assertEquals(0, seat.getColumn());
    }

}
