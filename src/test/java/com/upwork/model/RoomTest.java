package com.upwork.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class RoomTest {

    @Test
    void createRoom() {
        Room room = new Room(15, 11);
        int seatsQuantity = 15*11;
        assertEquals(15, room.getRows());
        assertEquals(11, room.getColumns());
        assertNotNull(room.getSeats());
        assertEquals(seatsQuantity, room.getTotalSeats());
    }

    @Test
    void createDefaultRoom() {
        Room room = new Room();
        assertEquals(10, room.getRows());
        assertEquals(10, room.getColumns());
    }

}