package com.upwork.model;

import com.upwork.exeption.NoFreeSpotsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ArrayRoomTest {

    @Test
    void createAndCheckAnArrayRoomDefaultValues() {
        ArrayRoom arrayRoom = new ArrayRoom();
        assertEquals(10, arrayRoom.getRowCount());
        assertEquals(100, arrayRoom.getTotalSeats());
    }

    @Test
    void reserveASeatAndCheckTheSpot() {
        ArrayRoom arrayRoom = new ArrayRoom();
        int[] reservedSpot = arrayRoom.reserve();
        int[] expectedReservedSpot = {0, 0};
        assertArrayEquals(expectedReservedSpot, reservedSpot);
    }

    @Test
    void reserveTwoSeatsAndCheckTheSpots() {
        ArrayRoom arrayRoom = new ArrayRoom();
        int[] reservedSpot = arrayRoom.reserve();
        int[] expectedReservedSpot = {0, 0};
        int[] anotherReservedSpot = arrayRoom.reserve();
        int[] expectedAnotherReservedSpot = {0, 2};
        assertArrayEquals(expectedReservedSpot, reservedSpot);
        assertArrayEquals(expectedAnotherReservedSpot, anotherReservedSpot);
        arrayRoom.printSeats();
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10, 50})
    void reserveSpots(int tickets) {
        ArrayRoom arrayRoom = new ArrayRoom();
        int i = 0;
        while (i < tickets) {
            int currentRow = i / 5;
            int currentColumn = ((i * 2 + 1 % 10) - 1) % 10;
            arrayRoom.reserve();
            assertEquals(1, arrayRoom.getPlanning()[currentRow][currentColumn]);
            i++;
        }
        arrayRoom.printSeats();
    }

    @Test
    void reserveFiftyOneSeatsAndCheckTheSpots() {
        ArrayRoom arrayRoom = new ArrayRoom();
        int i = 0;
        while (i < 50) {
            int currentRow = i / 5;
            int currentColumn = ((i * 2 + 1 % 10) - 1) % 10;
            arrayRoom.reserve();
            assertEquals(1, arrayRoom.getPlanning()[currentRow][currentColumn]);
            i++;
        }
        assertThrows(NoFreeSpotsException.class, arrayRoom::reserve);
        arrayRoom.printSeats();
    }

    @Test
    void reserveAny() {
        ArrayRoom arrayRoom = new ArrayRoom();
        int[] reserve = arrayRoom.reserve(3, 5);
        int[] expectedReverse = {3, 5};
        assertArrayEquals(expectedReverse, reserve);
        assertEquals(97, arrayRoom.getFreeSpots());
        arrayRoom.printSeats();
    }

    @Test
    void reserveTopRightSeat() {
        ArrayRoom arrayRoom = new ArrayRoom();
        int[] reserve = arrayRoom.reserve(9, 9);
        int[] expectedReverse = {9, 9};
        assertArrayEquals(expectedReverse, reserve);
        assertEquals(98, arrayRoom.getFreeSpots());
        arrayRoom.printSeats();
    }

}